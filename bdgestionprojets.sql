-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 25 fév. 2019 à 01:31
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bdgestionprojets`
--

-- --------------------------------------------------------

--
-- Structure de la table `composer`
--

DROP TABLE IF EXISTS `composer`;
CREATE TABLE IF NOT EXISTS `composer` (
  `idEquipe` int(11) NOT NULL,
  `emailDev` varchar(100) NOT NULL,
  PRIMARY KEY (`idEquipe`,`emailDev`),
  KEY `emailDev` (`emailDev`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `composer`
--

INSERT INTO `composer` (`idEquipe`, `emailDev`) VALUES
(12, 'dev1@dev1.com'),
(12, 'dev2@dev2.com'),
(12, 'dev3@dev3.com');

-- --------------------------------------------------------

--
-- Structure de la table `contrat`
--

DROP TABLE IF EXISTS `contrat`;
CREATE TABLE IF NOT EXISTS `contrat` (
  `num` int(11) NOT NULL,
  `dateSign` date NOT NULL,
  `dateFin` int(11) DEFAULT NULL,
  `codeProj` int(11) NOT NULL,
  PRIMARY KEY (`num`),
  KEY `codeProj` (`codeProj`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `contrat`
--

INSERT INTO `contrat` (`num`, `dateSign`, `dateFin`, `codeProj`) VALUES
(1, '2019-02-28', NULL, 18);

-- --------------------------------------------------------

--
-- Structure de la table `developper`
--

DROP TABLE IF EXISTS `developper`;
CREATE TABLE IF NOT EXISTS `developper` (
  `emailDev` varchar(100) NOT NULL,
  `codeTache` int(11) NOT NULL,
  `nbHeures` int(11) DEFAULT NULL,
  PRIMARY KEY (`emailDev`,`codeTache`),
  KEY `codeTache` (`codeTache`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `developper`
--

INSERT INTO `developper` (`emailDev`, `codeTache`, `nbHeures`) VALUES
('dev1@dev1.com', 4, 5),
('dev2@dev2.com', 5, NULL),
('dev3@dev3.com', 5, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `entreprise`
--

DROP TABLE IF EXISTS `entreprise`;
CREATE TABLE IF NOT EXISTS `entreprise` (
  `siren` varchar(100) NOT NULL,
  `denomination` varchar(255) NOT NULL,
  `adresse` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `emailUtilisateur` varchar(100) NOT NULL,
  PRIMARY KEY (`siren`),
  KEY `emailUtilisateur` (`emailUtilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `entreprise`
--

INSERT INTO `entreprise` (`siren`, `denomination`, `adresse`, `telephone`, `emailUtilisateur`) VALUES
('365412365', 'Entreprise1', '9 bis 4 rue', '0321456987', 'ets1@ets1.com');

-- --------------------------------------------------------

--
-- Structure de la table `equipe`
--

DROP TABLE IF EXISTS `equipe`;
CREATE TABLE IF NOT EXISTS `equipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) NOT NULL,
  `emailResp` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `emailResp` (`emailResp`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `equipe`
--

INSERT INTO `equipe` (`id`, `libelle`, `emailResp`) VALUES
(12, 'Equipe 1', 'dev1@dev1.com');

-- --------------------------------------------------------

--
-- Structure de la table `maitriser`
--

DROP TABLE IF EXISTS `maitriser`;
CREATE TABLE IF NOT EXISTS `maitriser` (
  `emailDev` varchar(100) NOT NULL,
  `codeOutil` int(11) NOT NULL,
  PRIMARY KEY (`emailDev`,`codeOutil`),
  KEY `codeOutil` (`codeOutil`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `maitriser`
--

INSERT INTO `maitriser` (`emailDev`, `codeOutil`) VALUES
('dev2@dev2.com', 6),
('dev2@dev2.com', 7),
('dev2@dev2.com', 10),
('dev1@dev1.com', 12),
('dev1@dev1.com', 13);

-- --------------------------------------------------------

--
-- Structure de la table `outils`
--

DROP TABLE IF EXISTS `outils`;
CREATE TABLE IF NOT EXISTS `outils` (
  `code` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) NOT NULL,
  `version` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `outils`
--

INSERT INTO `outils` (`code`, `libelle`, `version`) VALUES
(6, 'Symfony ', '3'),
(7, 'Symfony', '4.1 '),
(8, 'Phaser', '3.13'),
(9, 'Phaser ', '2.27'),
(10, 'NetBeans', '8.2'),
(11, 'Android Studio', '3.2.1'),
(12, 'Photoshop', 'CC 2018'),
(13, 'Sony Vegas', '16');

-- --------------------------------------------------------

--
-- Structure de la table `projet`
--

DROP TABLE IF EXISTS `projet`;
CREATE TABLE IF NOT EXISTS `projet` (
  `code` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `statut` int(11) NOT NULL,
  `siren` varchar(100) NOT NULL,
  `idEquipe` int(11) DEFAULT NULL,
  PRIMARY KEY (`code`),
  KEY `idEquipe` (`idEquipe`),
  KEY `statut` (`statut`),
  KEY `siren` (`siren`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `projet`
--

INSERT INTO `projet` (`code`, `libelle`, `description`, `statut`, `siren`, `idEquipe`) VALUES
(18, 'Projet 1', 'Je voudrais un projet svp', 1, '365412365', 12),
(19, 'Projet 2', 'Je veux un projet svp', 2, '365412365', NULL),
(20, 'Projet 3', 'donne moi un projet svp', 3, '365412365', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL,
  `libelle` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`id`, `libelle`) VALUES
(1, 'Administrateur'),
(2, 'Développeur'),
(3, 'Entreprise');

-- --------------------------------------------------------

--
-- Structure de la table `statut`
--

DROP TABLE IF EXISTS `statut`;
CREATE TABLE IF NOT EXISTS `statut` (
  `id` int(11) NOT NULL,
  `libelle` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `statut`
--

INSERT INTO `statut` (`id`, `libelle`) VALUES
(1, 'Validé'),
(2, 'En attente'),
(3, 'Refusé');

-- --------------------------------------------------------

--
-- Structure de la table `tache`
--

DROP TABLE IF EXISTS `tache`;
CREATE TABLE IF NOT EXISTS `tache` (
  `code` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) NOT NULL,
  `commentaire` text NOT NULL,
  `dateDeb` date NOT NULL,
  `dateFin` date NOT NULL,
  `codeProj` int(11) NOT NULL,
  PRIMARY KEY (`code`),
  KEY `Tache_ibfk_1` (`codeProj`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tache`
--

INSERT INTO `tache` (`code`, `libelle`, `commentaire`, `dateDeb`, `dateFin`, `codeProj`) VALUES
(4, 'Creation du visuel', 'css', '2018-11-14', '2018-11-21', 18),
(5, 'Codage PHP', 'PHP', '2018-11-14', '2018-11-21', 18);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `email` varchar(100) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `adresse` varchar(255) NOT NULL,
  `coutHoraire` float DEFAULT NULL,
  `mdp` varchar(255) NOT NULL,
  `idRole` int(11) NOT NULL,
  PRIMARY KEY (`email`),
  KEY `idRole` (`idRole`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`email`, `nom`, `prenom`, `telephone`, `adresse`, `coutHoraire`, `mdp`, `idRole`) VALUES
('admin@admin.com', 'Admin', 'Admin', '0254856324', '4 rue 9 bis', NULL, '$2y$10$njC4WkZ8x/79a/KsYLylSenw5EGGGYQs8CAVGzbmDvxkrgHXRvQOG', 1),
('dev1@dev1.com', 'dev1', 'dev1', '0777563255', '45 rue de champ', 9.95, '$2y$10$WSC/Zt4YXXezRboYHHhPN.kIVcyLBRmQc.fHKhVBAHtPlUTmVeCeS', 2),
('dev2@dev2.com', 'dev2', 'dev2', '0655263256', '52 rue du moulin', 9.85, '$2y$10$.V14/51p2IE297Rz.rQux.ao96WFOhvU4MuHyVRS07TtXxZNAzaQa', 2),
('dev3@dev3.com', 'dev3', 'dev3', '0563245865', '12 rue michel drucker', 9.65, '$2y$10$bvz/wzteGTDbj5BzWN/UXuBXKQWyasjsISxN5pf1HxeQp1f/SkpZK', 2),
('developpeur@developpeur.com', 'Dev', 'Dev', '0123654789', '4rue nice', 5, '$2y$10$T2.k1aTjWoSQaCawrPGQCeveHnXCKODnhbD1Uw5VKIo8i6gIH6tFu', 2),
('ets1@ets1.com', 'ets1', 'ets1', '0563245865', '57 rue des quatres maisons', NULL, '$2y$10$7xbz9pMtUaEaopaqLGeah.2V7HQFYJqbbIS.ij8.UJD/rfCgNnW1G', 3);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `composer`
--
ALTER TABLE `composer`
  ADD CONSTRAINT `Composer_ibfk_1` FOREIGN KEY (`idEquipe`) REFERENCES `equipe` (`id`),
  ADD CONSTRAINT `Composer_ibfk_2` FOREIGN KEY (`emailDev`) REFERENCES `utilisateur` (`email`);

--
-- Contraintes pour la table `contrat`
--
ALTER TABLE `contrat`
  ADD CONSTRAINT `Contrat_ibfk_1` FOREIGN KEY (`codeProj`) REFERENCES `projet` (`code`);

--
-- Contraintes pour la table `developper`
--
ALTER TABLE `developper`
  ADD CONSTRAINT `Developper_ibfk_2` FOREIGN KEY (`codeTache`) REFERENCES `tache` (`code`),
  ADD CONSTRAINT `Developper_ibfk_3` FOREIGN KEY (`emailDev`) REFERENCES `utilisateur` (`email`);

--
-- Contraintes pour la table `entreprise`
--
ALTER TABLE `entreprise`
  ADD CONSTRAINT `Entreprise_ibfk_1` FOREIGN KEY (`emailUtilisateur`) REFERENCES `utilisateur` (`email`);

--
-- Contraintes pour la table `equipe`
--
ALTER TABLE `equipe`
  ADD CONSTRAINT `Equipe_ibfk_1` FOREIGN KEY (`emailResp`) REFERENCES `utilisateur` (`email`);

--
-- Contraintes pour la table `maitriser`
--
ALTER TABLE `maitriser`
  ADD CONSTRAINT `Maitriser_ibfk_1` FOREIGN KEY (`emailDev`) REFERENCES `utilisateur` (`email`),
  ADD CONSTRAINT `Maitriser_ibfk_2` FOREIGN KEY (`codeOutil`) REFERENCES `outils` (`code`);

--
-- Contraintes pour la table `projet`
--
ALTER TABLE `projet`
  ADD CONSTRAINT `Projet_ibfk_2` FOREIGN KEY (`idEquipe`) REFERENCES `equipe` (`id`),
  ADD CONSTRAINT `Projet_ibfk_3` FOREIGN KEY (`statut`) REFERENCES `statut` (`id`),
  ADD CONSTRAINT `Projet_ibfk_4` FOREIGN KEY (`siren`) REFERENCES `entreprise` (`siren`);

--
-- Contraintes pour la table `tache`
--
ALTER TABLE `tache`
  ADD CONSTRAINT `Tache_ibfk_1` FOREIGN KEY (`codeProj`) REFERENCES `projet` (`code`);

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `Utilisateur_ibfk_1` FOREIGN KEY (`idRole`) REFERENCES `role` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
