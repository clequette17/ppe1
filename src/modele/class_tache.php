<?php

class Tache {

    private $db;
    private $select;
    private $insert;
    private $selectMaxCode;
    private $insertDevelopper;
    private $selectByCodeProj;
    private $deleteDevelopper;
    private $deleteDevelopperAll;
    private $delete;
    private $selectMesTaches;
    private $updateDevelopper;
    private $selectByCode;
    private $update;
    private $selectDevsByCode;
    private $selectNotDevsByCode;

    public function __construct($db) {
        $this->db = $db;
        $this->select = $db->prepare("select * from Role order by libelle");
        $this->insert = $db->prepare("insert into Tache(libelle, commentaire, dateDeb, dateFin, codeProj) values (:libelle, :commentaire, :dateDeb, :dateFin, :codeProj)");
        $this->selectMaxCode = $db->prepare("select max(code) as maxCode from Tache");
        $this->insertDevelopper = $db->prepare("insert into Developper(emailDev, codeTache) values(:emailDev, :codeTache)");
        $this->selectByCodeProj = $db->prepare("select * from Tache where codeProj=:codeProj");
        $this->deleteDevelopper = $db->prepare("delete from Developper where emailDev=:emailDev and codeTache=:codeTache");
        $this->deleteDevelopperAll = $db->prepare("delete from Developper where codeTache=:codeTache");
        $this->delete = $db->prepare("delete from Tache where code=:code");
        $this->selectMesTaches = $db->prepare("select t.*, p.libelle as libelleProj, idEquipe from Tache t inner join Developper d on t.code=d.codeTache inner join Utilisateur u on d.emailDev=u.email inner join Projet p on t.codeProj=p.code where email=:email order by t.code DESC");
        $this->updateDevelopper = $db->prepare("update Developper set nbHeures=:heure where emailDev=:email and codeTache=:code");
        $this->selectByCode = $db->prepare("select * from Tache where code = :code");
        $this->update = $db->prepare("update Tache set libelle = :libelle, commentaire = :commentaire, dateDeb = :dateDeb, dateFin = :dateFin where code = :code");
        $this->selectDevsByCode = $db->prepare("select * from Developper d inner join Utilisateur u on d.emailDev = u.email where codeTache = :code order by nom, prenom");
        $this->selectNotDevsByCode = $db->prepare("select * from Utilisateur where email not in (select emailDev from Developper where codeTache = :code) and idRole = 2 order by nom, prenom");

    }

    public function select() {
        $this->select->execute();
        if ($this->select->errorCode() != 0) {
            print_r($this->select->errorInfo());
        }
        return $this->select->fetchAll();
    }

    public function insert($libelle, $commentaire, $dateDeb, $dateFin, $codeProj) {
        $r = true;
        $this->insert->execute(array(':libelle' => $libelle, ':commentaire' => $commentaire, ':dateDeb' => $dateDeb, ':dateFin' => $dateFin, ':codeProj' => $codeProj));
        if ($this->insert->errorCode() != 0) {
            print_r($this->insert->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function selectMaxCode() {
        $this->selectMaxCode->execute();
        if ($this->selectMaxCode->errorCode() != 0) {
            print_r($this->selectMaxCode->errorInfo());
        }
        return $this->selectMaxCode->fetch();
    }

    public function insertDevelopper($emailDev, $codeTache) {
        $r = true;
        $this->insertDevelopper->execute(array(':emailDev' => $emailDev, ':codeTache' => $codeTache));
        if ($this->insertDevelopper->errorCode() != 0) {
            print_r($this->insertDevelopper->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function selectByCodeProj($codeProj) {
        $this->selectByCodeProj->execute(array(':codeProj' => $codeProj));
        if ($this->selectByCodeProj->errorCode() != 0) {
            print_r($this->selectByCodeProj->errorInfo());
        }
        return $this->selectByCodeProj->fetchAll();
    }

    public function deleteDevelopper($emailDev, $codeTache) {
        $r = true;
        $this->deleteDevelopper->execute(array(':emailDev' => $emailDev, ':codeTache' => $codeTache));
        if ($this->deleteDevelopper->errorCode() != 0) {
            print_r($this->deleteDevelopper->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function deleteDevelopperAll($codeTache) {
        $r = true;
        $this->deleteDevelopperAll->execute(array(':codeTache' => $codeTache));
        if ($this->deleteDevelopperAll->errorCode() != 0) {
            print_r($this->deleteDevelopperAll->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function delete($code) {
        $r = true;
        $this->delete->execute(array(':code' => $code));
        if ($this->delete->errorCode() != 0) {
            print_r($this->delete->errorInfo());
            $r = false;
        }
        return $r;
    }
    
        public function selectMesTaches($email) {
        $this->selectMesTaches->execute(array(':email'=>$email));
        if ($this->selectMesTaches->errorCode() != 0) {
            print_r($this->selectMesTaches->errorInfo());
        }
        return $this->selectMesTaches->fetchAll();
    }
    
        public function updateDevelopper($email, $code, $heure) {
        $r = true;
        $this->updateDevelopper->execute(array(':email' => $email, ':code' => $code, ':heure' => $heure));
        if ($this->updateDevelopper->errorCode() != 0) {
            print_r($this->updateDevelopper->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function selectByCode($code) {
        $this->selectByCode->execute(array(':code' => $code));
        if ($this->selectByCode->errorCode() != 0) {
            print_r($this->selectByCode->errorInfo());
        }
        return $this->selectByCode->fetch();
    }

    public function update($libelle, $commentaire, $dateDeb, $dateFin, $code) {
        $r = true;
        $this->update->execute(array(':libelle' => $libelle, ':commentaire' => $commentaire, ':dateDeb' => $dateDeb, ':dateFin' => $dateFin, ':code' => $code));
        if ($this->update->errorCode() != 0) {
            print_r($this->update->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function selectDevsByCode($code) {
        $this->selectDevsByCode->execute(array(':code' => $code));
        if ($this->selectDevsByCode->errorCode() != 0) {
            print_r($this->selectDevsByCode->errorInfo());
        }
        return $this->selectDevsByCode->fetchAll();
    }

    public function selectNotDevsByCode($code) {
        $this->selectNotDevsByCode->execute(array(':code' => $code));
        if ($this->selectNotDevsByCode->errorCode() != 0) {
            print_r($this->selectNotDevsByCode->errorInfo());
        }
        return $this->selectNotDevsByCode->fetchAll();
    }

}

?>
