<?php

class Outils {

    private $db;
    private $select;
    private $insert;
    private $delete;
    private $selectByCode;
    private $update;
    private $insertMaitriser;
    private $selectLesOutils;
    private $deleteUnOutil;
    private $deleteToutOutil;

    public function __construct($db) {
        $this->db = $db;
        $this->select = $db->prepare("select * from Outils order by libelle");
        $this->insert = $db->prepare("insert into Outils(libelle, version) values (:libelle, :version)");
        $this->delete = $db->prepare("delete from Outils where code=:code");
        $this->selectByCode = $db->prepare("select * from Outils where code=:code");
        $this->update = $db->prepare("update Outils set libelle=:libelle, version=:version where code=:code");
        $this->insertMaitriser = $db->prepare("insert into Maitriser values(:emailDev, :codeOutil)");
        $this->selectLesOutils = $db->prepare("select * from Maitriser m inner join Outils o on m.codeOutil=o.code where emailDev=:emailDev order by libelle");
        $this->deleteUnOutil = $db->prepare("delete from Maitriser where emailDev=:emailDev and codeOutil=:codeOutil");
        $this->deleteToutOutil = $db->prepare("delete from Maitriser where codeOutil=:codeOutil");
    }

    public function select() {
        $this->select->execute();
        if ($this->select->errorCode() != 0) {
            print_r($this->select->errorInfo());
        }
        return $this->select->fetchAll();
    }

    public function insert($libelle, $version) {
        $r = true;
        $this->insert->execute(array(':libelle' => $libelle, ':version' => $version));
        if ($this->insert->errorCode() != 0) {
            print_r($this->insert->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function delete($code) {
        $r = true;
        $this->delete->execute(array(':code' => $code));
        if ($this->delete->errorCode() != 0) {
            print_r($this->delete->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function selectByCode($code) {
        $this->selectByCode->execute(array(':code' => $code));
        if ($this->selectByCode->errorCode() != 0) {
            print_r($this->selectByCode->errorInfo());
        }
        return $this->selectByCode->fetch();
    }

    public function update($code, $libelle, $version) {
        $r = true;
        $this->update->execute(array(':code' => $code, ':libelle' => $libelle, ':version' => $version));
        if ($this->update->errorCode() != 0) {
            print_r($this->update->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function insertMaitriser($emailDev, $codeOutil) {
        $r = true;
        $this->insertMaitriser->execute(array(':emailDev' => $emailDev, ':codeOutil' => $codeOutil));
        if ($this->insertMaitriser->errorCode() != 0) {
            print_r($this->insertMaitriser->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function selectLesOutils($emailDev) {
        $this->selectLesOutils->execute(array(':emailDev' => $emailDev));
        if ($this->selectLesOutils->errorCode() != 0) {
            print_r($this->selectLesOutils->errorInfo());
        }
        return $this->selectLesOutils->fetchAll();
    }

    public function deleteUnOutil($emailDev, $codeOutil) {
        $r = true;
        $this->deleteUnOutil->execute(array(':emailDev' => $emailDev, 'codeOutil' => $codeOutil));
        if ($this->deleteUnOutil->errorCode() != 0) {
            print_r($this->deleteUnOutil->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function deleteToutOutil($codeOutil) {
        $r = true;
        $this->deleteToutOutil->execute(array('codeOutil' => $codeOutil));
        if ($this->deleteToutOutil->errorCode() != 0) {
            print_r($this->deleteToutOutil->errorInfo());
            $r = false;
        }
        return $r;
    }

}

?>
