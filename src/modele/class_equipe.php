<?php

class Equipe {

    private $db;
    private $insert;
    private $select;
    private $delete;
    private $update;
    private $selectById;
    private $selectByEmailResp;
    private $selectLesDevs;
    private $selectNotLesDevs;
    private $insertDev;
    private $deleteDev;

    public function __construct($db) {
        $this->db = $db;
        $this->insert = $db->prepare("insert into Equipe(libelle, emailResp) values (:libelle, :responsable)");
        $this->select = $db->prepare("select id, libelle, u.nom, u.prenom from Equipe e inner join Utilisateur u on e.emailResp = u.email  order by libelle");
        $this->delete = $db->prepare("delete from Equipe where id=:id");
        $this->update = $db->prepare("update Equipe set libelle=:libelle, emailResp=:responsable where id=:id");
        $this->selectById = $db->prepare("select * from Equipe e inner join Utilisateur u on e.emailResp=u.email where id=:id");
        $this->selectByEmailResp = $db->prepare("select * from Equipe where emailResp=:emailResp");
        $this->selectLesDevs = $db->prepare("select * from Composer c inner join Utilisateur u on c.emailDev=u.email where idEquipe=:id");
        $this->selectNotLesDevs = $db->prepare("select * from Composer c inner join Utilisateur u on c.emailDev=u.email where idEquipe=:id and c.emailDev not in (select emailDev from Developper where codeTache=:codeTache)");
        $this->insertDev = $db->prepare("insert into Composer values(:idEquipe, :emailDev)");
        $this->deleteDev = $db->prepare("delete from Composer where idEquipe=:idEquipe and emailDev=:emailDev");
    }

    public function insert($libelle, $responsable) {
        $r = true;
        $this->insert->execute(array(':libelle' => $libelle, ':responsable' => $responsable));
        if ($this->insert->errorCode() != 0) {
            print_r($this->insert->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function select() {
        $this->select->execute();
        if ($this->select->errorCode() != 0) {
            print_r($this->select->errorInfo());
        }
        return $this->select->fetchAll();
    }

    public function selectByEmailResp($emailResp) {
        $this->selectByEmailResp->execute(array(':emailResp' => $emailResp));
        if ($this->selectByEmailResp->errorCode() != 0) {
            print_r($this->selectByEmailResp->errorInfo());
        }
        return $this->selectByEmailResp->fetchAll();
    }

    public function update($id, $libelle, $responsable) {
        $r = true;
        $this->update->execute(array(':id' => $id, ':libelle' => $libelle, ':responsable' => $responsable));
        if ($this->update->errorCode() != 0) {
            print_r($this->update->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function selectById($id) {
        $this->selectById->execute(array(':id' => $id));
        if ($this->selectById->errorCode() != 0) {
            print_r($this->selectById->errorInfo());
        }
        return $this->selectById->fetch();
    }

    public function delete($id) {
        $r = true;
        $this->delete->execute(array(':id' => $id));
        if ($this->delete->errorCode() != 0) {
            print_r($this->delete->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function selectLesDevs($id) {
        $this->selectLesDevs->execute(array(':id' => $id));
        if ($this->selectLesDevs->errorCode() != 0) {
            print_r($this->selectLesDevs->errorInfo());
        }
        return $this->selectLesDevs->fetchAll();
    }

    public function selectNotLesDevs($id, $codeTache) {
        $this->selectNotLesDevs->execute(array(':id' => $id, ':codeTache' => $codeTache));
        if ($this->selectNotLesDevs->errorCode() != 0) {
            print_r($this->selectNotLesDevs->errorInfo());
        }
        return $this->selectNotLesDevs->fetchAll();
    }

    public function insertDev($idEquipe, $emailDev) {
        $r = true;
        $this->insertDev->execute(array(':idEquipe' => $idEquipe, ':emailDev' => $emailDev));
        if ($this->insertDev->errorCode() != 0) {
            print_r($this->insertDev->errorInfo());
            $r = false;
        }
        return $r;
    }
    
        public function deleteDev($idEquipe, $emailDev) {
        $r = true;
        $this->deleteDev->execute(array(':idEquipe' => $idEquipe, ':emailDev' => $emailDev));
        if ($this->deleteDev->errorCode() != 0) {
            print_r($this->deleteDev->errorInfo());
            $r = false;
        }
        return $r;
    }

}
?>



