<?php

class Utilisateur{
    
    private $db;
    private $insert;
    private $connect;
    private $select;
    private $selectByEmail;
    private $update;
    private $updateMdp;
    private $delete;
    private $selectDev;
    
    public function __construct($db){
        $this->db = $db;
        $this->insert = $db->prepare("insert into Utilisateur(email, mdp, nom, prenom, idRole, telephone, adresse, coutHoraire) values (:email, :mdp, :nom, :prenom, :role, :telephone, :adresse, :cout)");    
        $this->connect = $db->prepare("select email, idRole, mdp from Utilisateur where email=:email");
        $this->select = $db->prepare("select * from Utilisateur u inner join Role r on u.idRole=r.id");
        $this->selectByEmail = $db->prepare("select * from Utilisateur where email=:email");
        $this->update = $db->prepare("update Utilisateur set nom=:nom, prenom=:prenom, telephone=:telephone, adresse=:adresse, coutHoraire=:cout, idRole=:role where email=:email");
        $this->updateMdp = $db->prepare("update Utilisateur set mdp=:mdp where email=:email");
        $this->delete = $db->prepare("delete from Utilisateur where email=:email");
        $this->selectDev = $db->prepare("select * from Utilisateur where idRole=2");
        }
    public function insert($email, $mdp, $role, $nom, $prenom, $telephone, $adresse, $cout){
        $r = true;
        $this->insert->execute(array(':email'=>$email, ':mdp'=>$mdp, ':role'=>$role, ':nom'=>$nom, ':prenom'=>$prenom, ':telephone'=>$telephone, ':adresse'=>$adresse, ':cout'=>$cout));
        if ($this->insert->errorCode()!=0){
             print_r($this->insert->errorInfo());  
             $r=false;
        }
        return $r;
    }
    
    public function connect($email){  
        $unUtilisateur = $this->connect->execute(array(':email'=>$email));
        if ($this->connect->errorCode()!=0){
             print_r($this->connect->errorInfo());  
        }
        return $this->connect->fetch();
    }
    
    public function select(){
        $this->select->execute();
        if ($this->select->errorCode()!=0){
             print_r($this->select->errorInfo());  
        }
        return $this->select->fetchAll();
    }
    
    public function selectByEmail($email){ 
        $this->selectByEmail->execute(array(':email'=>$email)); 
        if ($this->selectByEmail->errorCode()!=0){
            print_r($this->selectByEmail->errorInfo()); 
            
        }
        return $this->selectByEmail->fetch(); 
    }
    
    public function update($email, $nom, $prenom, $telephone, $adresse, $cout, $role){
        $r = true;
        $this->update->execute(array(':email'=>$email, ':nom'=>$nom, ':prenom'=>$prenom, ':telephone' => $telephone, ':adresse' => $adresse, ':cout' => $cout, ':role' => $role));
        if ($this->update->errorCode()!=0){
             print_r($this->update->errorInfo());  
             $r=false;
        }
        return $r;
    }
    
    public function updateMdp($email, $mdp){
        $r = true;
        $this->updateMdp->execute(array(':email'=>$email, ':mdp'=>$mdp));
        if ($this->update->errorCode()!=0){
             print_r($this->updateMdp->errorInfo());  
             $r=false;
        }
        return $r;
    }
    public function delete($email){
        $r = true;
        $this->delete->execute(array(':email'=>$email));
        if ($this->delete->errorCode()!=0){
             print_r($this->delete->errorInfo());  
             $r=false;
        }
        return $r;
    }
    
        public function selectDev(){
        $this->selectDev->execute();
        if ($this->selectDev->errorCode()!=0){
             print_r($this->selectDev->errorInfo());  
        }
        return $this->selectDev->fetchAll();
    }
    
}

?>

