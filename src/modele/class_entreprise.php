<?php

class Entreprise {

    private $db;
    private $insert;
    private $selectSiren;
    private $selectByEmail;

    public function __construct($db) {
        $this->db = $db;
        $this->insert = $db->prepare("insert into Entreprise(siren, denomination, telephone, adresse, emailUtilisateur) values (:siren, :denomination, :telephone, :adresse, :email)");
        $this->selectSiren = $db->prepare('select siren from Entreprise where emailUtilisateur = :email');
        $this->selectByEmail = $db->prepare("select * from Entreprise where emailUtilisateur = :email");
    }

    public function insert($siren, $denomination, $telephone, $adresse, $email) {
        $r = true;
        $this->insert->execute(array(':siren' => $siren, ':denomination' => $denomination, ':telephone' => $telephone, ':adresse' => $adresse, ':email' => $email));
        if ($this->insert->errorCode() != 0) {
            print_r($this->insert->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function selectSiren($email) {

        $this->selectSiren->execute(array(':email' => $email));
        if ($this->selectSiren->errorCode() != 0) {
            print_r($this->selectSiren->errorInfo());
        }
        return $this->selectSiren->fetch();
    }
    public function selectByEmail($email) {

        $this->selectByEmail->execute(array(':email' => $email));
        if ($this->selectByEmail->errorCode() != 0) {
            print_r($this->selectByEmail->errorInfo());
        }
        return $this->selectByEmail->fetch();
    }

}
?>

