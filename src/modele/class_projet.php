<?php

class Projet {

    private $db;
    private $insert;
    private $selectBySiren;
    private $select;
    private $selectLesStatuts;
    private $updateStatut;
    private $updateEquipe;
    private $delete;
    private $selectByCode;
    private $selectByResp;
    private $deleteToutProjet;

    public function __construct($db) {
        $this->db = $db;
        $this->insert = $db->prepare("insert into Projet(libelle, description, statut, siren) values (:libelle, :desc, :statut, :siren)");
        $this->selectBySiren = $db->prepare("select p.*, s.libelle as libelleStatut, c.dateSign from Projet p inner join Statut s on p.statut = s.id left outer join Contrat c on p.code = c.codeProj where siren = :siren");
        $this->select = $db->prepare("select * from Projet order by code DESC");
        $this->selectLesStatuts = $db->prepare("select * from Statut");
        $this->updateStatut = $db->prepare("update Projet set statut=:statut where code=:code");
        $this->updateEquipe = $db->prepare("update Projet set idEquipe=:idEquipe where code=:code");
        $this->delete = $db->prepare("delete from Projet where code=:code");
        $this->selectByCode = $db->prepare("select * from Projet where code=:code");
        $this->selectByResp = $db->prepare("select p.* from Projet p inner join Equipe e on p.idEquipe=e.id where emailResp=:emailResp order by code DESC");
        $this->deleteToutProjet = $db->prepare("delete from Projet where code=:code ");
    }

    public function insert($libelle, $desc, $statut, $siren) {
        $r = true;
        $this->insert->execute(array(':libelle' => $libelle, ':desc' => $desc, ':statut' => $statut, ':siren' => $siren));
        if ($this->insert->errorCode() != 0) {
            print_r($this->insert->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function selectBySiren($siren) {

        $this->selectBySiren->execute(array(':siren' => $siren));
        if ($this->selectBySiren->errorCode() != 0) {
            print_r($this->selectBySiren->errorInfo());
        }
        return $this->selectBySiren->fetchAll();
    }

    public function select() {

        $this->select->execute();
        if ($this->select->errorCode() != 0) {
            print_r($this->select->errorInfo());
        }
        return $this->select->fetchAll();
    }

    public function selectLesStatuts() {

        $this->selectLesStatuts->execute();
        if ($this->selectLesStatuts->errorCode() != 0) {
            print_r($this->selectLesStatuts->errorInfo());
        }
        return $this->selectLesStatuts->fetchAll();
    }

    public function updateStatut($code, $statut) {
        $r = true;
        $this->updateStatut->execute(array(':code' => $code, ':statut' => $statut));
        if ($this->updateStatut->errorCode() != 0) {
            print_r($this->updateStatut->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function updateEquipe($code, $idEquipe) {
        $r = true;
        $this->updateEquipe->execute(array(':code' => $code, ':idEquipe' => $idEquipe));
        if ($this->updateEquipe->errorCode() != 0) {
            print_r($this->updateEquipe->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function delete($code) {
        $r = true;
        $this->delete->execute(array(':code' => $code));
        if ($this->delete->errorCode() != 0) {
            print_r($this->delete->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function selectByCode($code) {

        $this->selectByCode->execute(array(':code' => $code));
        if ($this->selectByCode->errorCode() != 0) {
            print_r($this->selectByCode->errorInfo());
        }
        return $this->selectByCode->fetch();
    }
    
        public function selectByResp($emailResp) {

        $this->selectByResp->execute(array(':emailResp' => $emailResp));
        if ($this->selectByResp->errorCode() != 0) {
            print_r($this->selectByResp->errorInfo());
        }
        return $this->selectByResp->fetchAll();
    }
    
        public function deleteToutProjet($code) {
        $r = true;
        $this->deleteToutProjet->execute(array('code' => $code));
        if ($this->deleteToutProjet->errorCode() != 0) {
            print_r($this->deleteToutProjet->errorInfo());
            $r = false;
        }
        return $r;
    }

}

?>