<?php

function actionUtilisateur($twig, $db)
{
    $form = array();
    $utilisateur = new Utilisateur($db);

    $role = new Role($db);

    $listeRole = $role->select();

    if (isset($_POST['btInscrire'])) {
        $email = $_POST['email'];
        $password = $_POST['password'];
        $password2 = $_POST['password2'];
        $nom = $_POST['nom'];
        $prenom = $_POST['prenom'];
        $role = $_POST['role'];
        $telephone = $_POST['telephone'];
        $adresse = $_POST['adresse'];
        $cout = $_POST['coutHoraire'];
        $form['valide'] = true;
        if ($password != $password2) {
            $form['valide'] = false;
            $form['message'] = 'Les mots de passe sont différents';
        } else {
            $utilisateur = new Utilisateur($db);
            $exec = $utilisateur->insert($email, password_hash($password, PASSWORD_DEFAULT), $role, $nom, $prenom, $telephone, $adresse, $cout);
            if (!$exec) {
                $form['valide'] = false;
                $form['message'] = 'Problème d\'insertion dans la table utilisateur ';
            }
        }
    }

    if (isset($_POST['btSupprimer'])) {
        $cocher = $_POST['cocher'];
        $form['valide'] = true;
        foreach ($cocher as $email) {
            $exec = $utilisateur->delete($email);
            if (!$exec) {
                $form['valide'] = false;
                $form['message'] = 'Problème de suppression dans la table utilisateur';
            }
        }
    }

    $liste = $utilisateur->select();
    echo $twig->render('utilisateur.html.twig', array('form' => $form, 'liste' => $liste, 'listeRole' => $listeRole));
}

function actionUtilisateurModif($twig, $db)
{
    $form = array();
    $utilisateur = new Utilisateur($db);
    $listeRole = array();
    $unUtilisateur = array();

    if (isset($_GET['email'])) {
        $unUtilisateur = $utilisateur->selectByEmail($_GET['email']);
        if ($unUtilisateur != null) {
            $role = new Role($db);
            $listeRole = $role->select();
        } else {
            $form['message'] = 'Utilisateur incorrect';
        }
    }

    if (isset($_POST['btModifier'])) {
        $utilisateur = new Utilisateur($db);
        $email = $_POST['email'];
        $nom = $_POST['nom'];
        $prenom = $_POST['prenom'];
        $telephone = $_POST['telephone'];
        $adresse = $_POST['adresse'];
        if (isset($_POST['cout']) && $_POST['cout'] > 0) {
            $cout = $_POST['cout'];
        } else {
            $cout = null;
        }

        $role = $_POST['role'];

        $exec = $utilisateur->update($email, $nom, $prenom, $telephone, $adresse, $cout, $role);
        if (!$exec) {
            $form['valide'] = false;
            $form['message'] = 'Echec de la modification des données. ';
        } else {
            $form['valide'] = true;
            $form['message'] = 'Modification des données réussie. ';
        }
        if (!empty($_POST['password'])) {
            $p1 = $_POST['password'];
            $p2 = $_POST['password2'];
            if ($p1 == $p2) {
                $p1 = password_hash($p1, PASSWORD_DEFAULT);
                $exec = $utilisateur->updateMdp($email, $p1);
                if (!$exec) {
                    $form['valide'] = false;
                    $form['message'] .= 'Echec de la modification du mot de passe';
                } else {
                    $form['valide'] = true;
                    $form['message'] .= 'Modification réussie du mot de passe';
                }
            } else {
                $form['valide'] = false;
                $form['message'] .= 'Echec de la modification du mot de passe';
            }
        }
    } else {
        $form['message'] = 'Utilisateur non précisé';
    }

    echo $twig->render('utilisateur-modif.html.twig', array('form' => $form, 'listeRole' => $listeRole, 'u' => $unUtilisateur));
}

function actionProjetClientVoir($twig, $db)
{
    $entreprise = new Entreprise($db);
    $uneentreprise = $entreprise->selectByEmail($_GET['email']);

    $projet = new Projet($db);
    $projets = $projet->selectBySiren($uneentreprise['siren']);

    if (isset($_POST['btDL'])) {
        $html = $twig->render('projetclient-voir_pdf.html.twig', array('entreprise' => $uneentreprise, 'projets' => $projets));
        try {
            ob_end_clean();
            $html2pdf = new \Spipu\Html2Pdf\Html2Pdf('P', 'A4', 'fr');
            $html2pdf->writeHTML($html);
            $html2pdf->output('projetsclient.pdf');
        } catch (Html2PdfException $e) {
            echo 'erreur ' . $e;
        }
    }

    echo $twig->render('projetclient-voir.html.twig', array('entreprise' => $uneentreprise, 'projets' => $projets, 'email' => $_GET['email']));

}
