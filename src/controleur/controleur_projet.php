<?php

function actionProjetAjout($twig, $db) {

    $form = array();

    if (isset($_POST['btAjouter'])) {

        $libelle = $_POST['libelle'];
        $desc = $_POST['desc'];
        $statut = 2;

        $entreprise = new Entreprise($db);

        $siren = $entreprise->selectSiren($_SESSION['login'])['siren'];

        $projet = new Projet($db);

        $exec = $projet->insert($libelle, $desc, $statut, $siren);

        $form['valide'] = true;

        if (!$exec) {
            $form['valide'] = false;
            $form['message'] = "Echec de l'ajout";
        }
    }

    echo $twig->render('projet-ajout.html.twig', array('form' => $form));
}

function actionProjetListe($twig, $db) {

    $projet = new Projet($db);

    $entreprise = new Entreprise($db);
    
    $siren = $entreprise->selectSiren($_SESSION['login'])['siren'];
    
    $liste = $projet->selectBySiren($siren);
    

    echo $twig->render("projet-liste.html.twig", array('liste' => $liste));
}

function actionProjet($twig, $db){
    
    $projet = new Projet($db);
    
    if(isset($_POST['btModifierS'])){
        
        $statut = $_POST['statut'];
        $code = $_POST['code'];
        
        $projet->updateStatut($code, $statut);
    }
    
    if(isset($_POST['btModifierE'])){
        
        $idEquipe = $_POST['equipe'];
        if($idEquipe == "null") $idEquipe = null;
        
        $code = $_POST['code'];
        
        $projet->updateEquipe($code, $idEquipe);
    }
    
    if(isset($_GET['code'])){
        
        $projet->delete($_GET['code']);
    }
    
    $liste = $projet->select();
    
    $listestatuts = $projet->selectLesStatuts();
    
    $equipe = new Equipe($db);
    $listeequipe = $equipe->select();
    
    if (isset($_POST['btSupprimer'])) {
        $cocher = $_POST['cocher'];
        $form['valide'] = true;
        foreach ($cocher as $code) {
            
            $exec = $projet->deleteToutProjet($code);
            $exec = $projet->delete($code);
            
            if (!$exec) {
                $form['valide'] = false;
                $form['message'] = 'Problème de suppression dans la table outils';
            }
        }
    }
    
    echo $twig->render("projet.html.twig", array('liste' => $liste, 'listestatuts' => $listestatuts, 'listeequipe' => $listeequipe));
}

function actionProjetVoir($twig, $db){
    
    $projet = new Projet($db);

    $liste = $projet->selectByResp($_SESSION['login']);
    
    echo $twig->render("projet-voir.html.twig", array('liste' => $liste));
}