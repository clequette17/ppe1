<?php

function actionEquipe($twig, $db) {
    $form = array();

    $equipe = new Equipe($db);

    $utilisateur = new Utilisateur($db);
    $listeresp = $utilisateur->selectDev();

    if (isset($_POST['btAjouter'])) {
        $libelle = $_POST['libelle'];
        $responsable = $_POST['responsable'];
        $form['valide'] = true;
        $exec = $equipe->insert($libelle, $responsable);
        if (!$exec) {
            $form['valide'] = false;
            $form['message'] = 'Problème d\'insertion dans la table équipe ';
        }
    }

    if (isset($_GET['id'])) {
        $exec = $equipe->delete($_GET['id']);
        if (!$exec) {
            $form['valide'] = false;
            $form['message'] = 'Problème de suppression dans la table équipe';
        } else {
            $form['valide'] = true;
        }
        $form['message'] = 'Equipe supprimée avec succès';
    }

    $liste = $equipe->select();
    echo $twig->render('equipe.html.twig', array('form' => $form, 'liste' => $liste, 'listeresp' => $listeresp));
}

function actionEquipeModif($twig, $db) {
    $form = array();
    if (isset($_GET['id'])) {
        $equipe = new Equipe($db);
        $uneEquipe = $equipe->selectById($_GET['id']);

        $utilisateur = new Utilisateur($db);
        $liste = $utilisateur->select();
    } else {
        $form['message'] = 'Equipe incorrecte';
    }
    if (isset($_POST['btModifier'])) {
        $id = $_POST['id'];
        $libelle = $_POST['libelle'];
        $responsable = $_POST['responsable'];
        $equipe = new Equipe($db);
        $exec = $equipe->update($id, $libelle, $responsable);
    }

    echo $twig->render('equipe-modif.html.twig', array('form' => $form, 'e' => $uneEquipe, 'liste' => $liste));
}

function actionEquipeAjout($twig, $db) {
    $form = array();
    if (isset($_POST['btAjouter'])) {
        $inputLibelle = $_POST['inputLibelle'];
        $inputIdResponsable = $_POST['inputIdResponsable'];
        $form['valide'] = true;
        $equipe = new Equipe($db);
        $exec = $equipe->insert($inputLibelle, $inputIdResponsable);
        if (!$exec) {
            $form['valide'] = false;
            $form['message'] = 'Problème d\'insertion dans la table équipe ';
        }
    } else {
        $utilisateur = new Utilisateur($db);
        $liste = $utilisateur->select();
        $form['liste'] = $liste;
    }

    echo $twig->render('equipe-ajout.html.twig', array('form' => $form));
}

function actionEquipeInfo($twig, $db) {

    $equipe = new Equipe($db);

    $utilisateur = new Utilisateur($db);
    $listedev = $utilisateur->selectDev();

    if (isset($_GET['id'])) {

        if (isset($_POST['btAjouter'])) {

            $dev = $_POST['dev'];

            $equipe->insertDev($_GET['id'], $dev);
        }

        if (isset($_GET['emailDev'])) {

            $equipe->deleteDev($_GET['id'], $_GET['emailDev']);
        }

        $uneequipe = $equipe->selectById($_GET['id']);

        $lesDevs = $equipe->selectLesDevs($_GET['id']);
    }

    echo $twig->render('equipe-info.html.twig', array('e' => $uneequipe, 'lesDevs' => $lesDevs, 'listedev' => $listedev));
}

function actionEquipeVoir($twig, $db) {

    $equipe = new Equipe($db);

    if (isset($_GET['id'])) {

        $uneequipe = $equipe->selectById($_GET['id']);
        $lesdevs = $equipe->selectLesDevs($_GET['id']);
    }

    echo $twig->render('equipe-voir.html.twig', array('e' => $uneequipe, 'lesdevs'=>$lesdevs));
}

// WebService
function actionEquipeWS($twig, $db) {
    $equipe = new Equipe($db);
    $json = json_encode($liste = $equipe->select());
    echo $json;
}
