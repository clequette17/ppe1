<?php

function actionOutils($twig, $db) {

    $outils = new Outils($db);
    
    if(isset($_POST['btAjouter'])){        
        $libelle = $_POST['libelle'];        
        if(isset($_POST['version'])){            
            $version = $_POST['version'];            
        }
        else $version = null;        
        $outils->insert($libelle, $version);
    }   

    if(isset($_GET['code'])){
        $outils->deleteToutOutil($_GET['code']);
        $outils->delete($_GET['code']);
    }   

    if (isset($_POST['btSupprimer'])) {
        $cocher = $_POST['cocher'];
        $form['valide'] = true;
        foreach ($cocher as $code) {
            
            $exec = $outils->deleteToutOutil($code);
            $exec = $outils->delete($code);
            
            if (!$exec) {
                $form['valide'] = false;
                $form['message'] = 'Problème de suppression dans la table outils';
            }
        }
    }

    $liste = $outils->select();   
    echo $twig->render('outils.html.twig', array('liste' => $liste));
}

function actionOutilsModif($twig, $db) {

    $outils = new Outils($db);
    
    $outil = array();
    
    if(isset($_GET['code'])){
        
        $outil = $outils->selectByCode($_GET['code']);
        
    }
    
    if(isset($_POST['btModifier'])){
        
        $libelle = $_POST['libelle'];
        $version = $_POST['version'];
        $code = $_POST['code'];
        
        $outils->update($code, $libelle, $version);
        
    }
    
    echo $twig->render('outils-modif.html.twig', array('o' => $outil));
}

function actionOutilsChoisir($twig, $db) {

    $outils = new Outils($db);
    
    $listeoutils = $outils->select();
    
    if(isset($_POST['btChoisir'])){
        
        $code = $_POST['code'];
        $email = $_SESSION['login'];
        
        $outils->insertMaitriser($email, $code);
        
    }
    
    if(isset($_GET['code'])){
        
        $outils->deleteUnOutil($_SESSION['login'], $_GET['code']);
    }
    
    $liste = $outils->selectLesOutils($_SESSION['login']);
    
    echo $twig->render('outils-choisir.html.twig', array('listeoutils' => $listeoutils, 'liste' => $liste));
}